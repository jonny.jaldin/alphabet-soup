import java.util.ArrayList;
import java.util.List;

public class PuzzleSearch {

	int start = 0;
	int end = 0;
	char[][] puzzle;
	int rows = 0;
	int cols = 0;
	List<String> positions = new ArrayList<String>();
	String searchWord = "";
	
	public PuzzleSearch(int start, int end, char[][] puzzle, String word) {
		super();
		this.start = start;
		this.end = end;
		this.puzzle = puzzle;
		this.rows = puzzle.length;
		this.cols = puzzle[0].length;
		this.searchWord = word;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public void setPositions(String s) {
		this.positions.add(s);
	}
	public int[][] direction() {
		int[][] directions = {
	            {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}
	        };
		return directions;
	}
	public boolean search(char[] word, int... currD) {
		if(word.length == 1) {
			System.out.println(this.searchWord+
					" "+this.positions.get(0)+" "+this.positions.get(this.positions.size()-1));
			return true;
		}
		int[][] d = this.direction();
		for(int k=0; k < word.length; k++) {
			char next = '.';
			if(k+1 != word.length) {
				next = word[k+1];
			}

			if( currD != null && currD.length != 0 ) {
				int rowIndex = this.start + currD[0];
				int colIndex = this.end + currD[1];
				if( next == '.') {
					break;
				} else if ((rowIndex >= 0 && rowIndex < rows) && (colIndex >= 0 && colIndex < cols)) {
					if( this.puzzle[rowIndex][colIndex] == next) {
						this.setStart(rowIndex);;
						this.setEnd(colIndex);
						//store current direction
						this.positions.add(rowIndex+":"+colIndex);
						int[] tmp = {currD[0], currD[1]};
						String tmpStr = new String(word);
						tmpStr = tmpStr.substring(1);
						return this.search(tmpStr.toCharArray(), tmp);
					}
				}
			} else {
				// loop through possible directions
				for(int i=0; i < d.length; i++) {
					int rowIndex = this.start + d[i][0];
					int colIndex = this.end + d[i][1];
					if( next == '.') {
						break;
					}
					if (rowIndex >= 0 && rowIndex < rows && colIndex >= 0 && colIndex < cols) {
						if( this.puzzle[rowIndex][colIndex] == next) {
							this.setStart(rowIndex);;
							this.setEnd(colIndex);
							//store current direction
							this.positions.add(rowIndex+":"+colIndex);
							int[] tmp = {d[i][0], d[i][1]};
							String tmpStr = new String(word);
							tmpStr = tmpStr.substring(1);
							return this.search(tmpStr.toCharArray(), tmp);
						}
					}
				}
			}
		}
		return false;
	}
}
