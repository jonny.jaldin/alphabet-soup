import java.io.*;
import java.util.*;

public class AlphabetSoup {
	
	char[][] puzzle;
	List<String> searchWords = new ArrayList<String>();

	public static void main(String[] args) {
		
		AlphabetSoup soup = new AlphabetSoup();
        try {
			soup.loadFile("src/resources/input.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	void loadFile(String filePath) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String[] dimensions = br.readLine().split("x");
            int rows = Integer.parseInt(dimensions[0]);
            int cols = Integer.parseInt(dimensions[1]);

            // Initialize the puzzle based on dimensions from the input
            puzzle = new char[rows][cols];
            
            // Populate the puzzle with characters from the file
            for (int i = 0; i < rows; i++) {
            	String line = br.readLine();
                String[] chars = line.split(" ");
                for (int j = 0; j < cols; j++) {
                    puzzle[i][j] = chars[j].charAt(0);
                }
            }
            
            String keyword;
            
            while ((keyword = br.readLine()) != null) {
            	searchWords.add(keyword);
            }
            searchWords.forEach((word) -> {
            	char[] wordArr = word.toCharArray();

            	// loop thru until find the first character,
            	// call search function if return true, continue with next ch
           		// if return false, continue searching puzzle for next match to key ch
           		for (int i = 0; i < rows; i++) {
           			for (int j = 0; j < cols; j++) {
           				if( puzzle[i][j] == wordArr[0] ) {
           					// search all 8 possible directions for next character
           					// starting at position i,j
           					// if they all match, check for next ch and narrow your directions to search
           					PuzzleSearch s = new PuzzleSearch(i, j, puzzle, word);
           					s.setPositions(i+":"+j);
           					if( s.search(wordArr) ) {
           						break;
           					}	
           				}
           			}
           		}
            });
        }
    }

}
